package patterns.behavioral.ChainOfResponsibility;

import patterns.behavioral.ChainOfResponsibility.chains.AutomaticHandler;
import patterns.behavioral.ChainOfResponsibility.chains.Operator;
import patterns.behavioral.ChainOfResponsibility.requests.ClientRequest;

public class Application {

    public static void main(String[] args) {

        ClientRequest clientRequest = new ClientRequest();
        AutomaticHandler automaticHandler = new AutomaticHandler();
        automaticHandler.handle(clientRequest);

        System.out.println("Is request handled? -> "+clientRequest.getStatus());

    }
}
