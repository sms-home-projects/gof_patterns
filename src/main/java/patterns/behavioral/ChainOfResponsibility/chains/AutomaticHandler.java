package patterns.behavioral.ChainOfResponsibility.chains;

import patterns.behavioral.ChainOfResponsibility.requests.ClientRequest;

public class AutomaticHandler implements Handler {
    @Override
    public boolean handle(ClientRequest clientRequest) {
        boolean isHandled = false;
        if (clientRequest == null) {
            isHandled = true;
        } else {
            if (clientRequest.getRequest() == null) {
                isHandled = true;
            } else {
                switch (clientRequest.getRequest()) {
                    case "1":
                        System.out.println("General information");
                        isHandled = true;
                        break;
                    case "2":
                        System.out.println("Information about services");
                        isHandled = true;
                        break;
                    case "3":
                        System.out.println("Now I connect you with operator");
                        setHandler(clientRequest);
                        isHandled = clientRequest.getStatus();
                        break;
                    default:
                        isHandled = true;
                        System.out.println("Bye bye! Have a nice day!");
                }
            }

        }
        String request = clientRequest.getRequest();
        clientRequest.setStatus(isHandled);
        return isHandled;
    }

    @Override
    public Handler setHandler(ClientRequest clientRequest) {
        Handler operator = new Operator();
        operator.handle(clientRequest);
        return operator;
    }


}
