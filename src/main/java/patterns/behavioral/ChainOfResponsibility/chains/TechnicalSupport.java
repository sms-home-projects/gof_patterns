package patterns.behavioral.ChainOfResponsibility.chains;

import patterns.behavioral.ChainOfResponsibility.requests.ClientRequest;

import java.util.Scanner;

public class TechnicalSupport implements Handler {

    @Override
    public boolean handle(ClientRequest clientRequest) {
        System.out.println("Support: 'Hello! I am listen to you' ");

        Scanner sc = new Scanner(System.in);
        String req = sc.next();
        clientRequest.setRequest(req);

        boolean randomAnswer = Math.random() > 0.5;
        if (randomAnswer) {
            System.out.println("Support: 'Ok. I can help you' ");
            clientRequest.setStatus(true);
        } else {
            System.out.println("Support: 'I can't help you. Be online." +
                    " I connect you with other specialist' ");
            setHandler(clientRequest);
        }
        return clientRequest.getStatus();
    }

    @Override
    public Handler setHandler(ClientRequest clientRequest) {
        TechnicalSupport technicalSupport = new TechnicalSupport();
        technicalSupport.handle(clientRequest);
        return technicalSupport;
    }


}
