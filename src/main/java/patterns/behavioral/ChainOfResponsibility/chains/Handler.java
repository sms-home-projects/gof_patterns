package patterns.behavioral.ChainOfResponsibility.chains;

import patterns.behavioral.ChainOfResponsibility.requests.ClientRequest;

public interface Handler {

    boolean handle(ClientRequest clientRequest);

    Handler setHandler(ClientRequest clientRequest);

}
