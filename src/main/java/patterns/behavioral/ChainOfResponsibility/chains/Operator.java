package patterns.behavioral.ChainOfResponsibility.chains;

import patterns.behavioral.ChainOfResponsibility.requests.ClientRequest;

import java.util.Scanner;

public class Operator implements Handler {
    private String name;

    public Operator() {
    }

    public Operator(String name) {
        this.name = name;
    }

    @Override
    public boolean handle(ClientRequest clientRequest) {
        System.out.println("Operator: 'Hello! I am listen to you' ");

        Scanner sc = new Scanner(System.in);
        String req = sc.next();
        clientRequest.setRequest(req);

        boolean randomAnswer = Math.random() > 0.5;
        if (randomAnswer) {
            System.out.println("Operator: 'Ok. I can help you' ");
            clientRequest.setStatus(true);
        } else {
            System.out.println("Operator: 'I can't help you. Be online." +
                    " I connect you with technical support' ");
            setHandler(clientRequest);
        }
        return clientRequest.getStatus();
    }

    @Override
    public Handler setHandler(ClientRequest clientRequest) {
        TechnicalSupport technicalSupport = new TechnicalSupport();
        technicalSupport.handle(clientRequest);
        return technicalSupport;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
