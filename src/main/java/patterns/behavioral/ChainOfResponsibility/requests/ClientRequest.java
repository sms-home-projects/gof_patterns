package patterns.behavioral.ChainOfResponsibility.requests;

import java.util.Scanner;

public class ClientRequest {
    private String request;
    private boolean status = false;

    public ClientRequest() {
        this.request = showMenu();
    }

    public ClientRequest(String request) {
        this.request = request;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String showMenu() {
        Scanner input = new Scanner(System.in);
        String keyMenu;
        System.out.println("Please, select menu point.");
        System.out.println("Press <<1>> for general information");
        System.out.println("Press <<2>> if you want to know more about services");
        System.out.println("Press <<3>> to connect with operator");
        keyMenu = input.nextLine();

        return keyMenu;

    }
}
