package patterns.behavioral.Observer;

import patterns.behavioral.Observer.users.YouTubeUser;
import patterns.behavioral.Observer.youTubeChannels.Video;
import patterns.behavioral.Observer.youTubeChannels.YouTubeChannel;

public class Application {
    public static void main(String[] args) {
        YouTubeUser user = new YouTubeUser("Ivan");
        user.setIs_SMS_Allowed(false);
        user.setIs_Email_Allowed(true);

        YouTubeChannel youTubeChannel = new YouTubeChannel("letsCode");

        user.subscribeToChannel(youTubeChannel);
        Video video = new Video();
        video.setHead("Simple HTTP server");
        video.setLink("https://www.youtube.com/watch?v=qwR6XsLaAY0");

        youTubeChannel.postNewVideo(video);

        user.showAllNotices();
    }
}
