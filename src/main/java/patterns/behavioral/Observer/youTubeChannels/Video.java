package patterns.behavioral.Observer.youTubeChannels;

public class Video {
    private String head;
    private String link;

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
