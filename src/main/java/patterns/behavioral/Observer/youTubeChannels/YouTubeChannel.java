package patterns.behavioral.Observer.youTubeChannels;

import patterns.behavioral.Observer.observers.Notifier;

import java.util.ArrayList;

public class YouTubeChannel {
    private String name;
    private final ArrayList<Video> myVideos = new ArrayList<>();
    private Notifier notifier = new Notifier();

    public YouTubeChannel(String name) {
        this.name = name;
    }

    public void postNewVideo(Video video) {
        myVideos.add(video);
        notifier.notify("New video " + video.getHead() +
                " on <<" + this.getName() + ">> You can watch it here: " + video.getLink());
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Video> getMyVideos() {
        return myVideos;
    }

    public Notifier getNotifier() {
        return notifier;
    }

    public void setNotifier(Notifier notifier) {
        this.notifier = notifier;
    }
}
