package patterns.behavioral.Observer.users;

import patterns.behavioral.Observer.youTubeChannels.YouTubeChannel;

import java.util.ArrayList;

public class YouTubeUser {
    private String nick;
    private boolean is_SMS_Allowed;
    private ArrayList<String> SMS_Notices = new ArrayList<>();
    private boolean is_Email_Allowed;
    private ArrayList<String> Email_Notices = new ArrayList<>();
    private ArrayList<String> UI_Notices = new ArrayList<>();

    public YouTubeUser() {
    }

    public YouTubeUser(String nick) {
        this.nick = nick;
    }

    public void update(String msg) {
        UI_Notices.add(msg);
        if (is_SMS_Allowed) {
            SMS_Notices.add(msg);
        }
        if (is_Email_Allowed) {
            Email_Notices.add(msg);
        }
    }

    public void showAllNotices() {
        System.out.println("UI: ");
        UI_Notices.forEach(System.out::println);
        System.out.println("SMS: ");
        SMS_Notices.forEach(System.out::println);
        System.out.println("EMAIL: ");
        Email_Notices.forEach(System.out::println);
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public boolean isIs_SMS_Allowed() {
        return is_SMS_Allowed;
    }

    public void setIs_SMS_Allowed(boolean is_SMS_Allowed) {
        this.is_SMS_Allowed = is_SMS_Allowed;
    }

    public ArrayList<String> getSMS_Notices() {
        return SMS_Notices;
    }

    public void setSMS_Notices(ArrayList<String> SMS_Notices) {
        this.SMS_Notices = SMS_Notices;
    }

    public boolean isIs_Email_Allowed() {
        return is_Email_Allowed;
    }

    public void setIs_Email_Allowed(boolean is_Email_Allowed) {
        this.is_Email_Allowed = is_Email_Allowed;
    }

    public ArrayList<String> getEmail_Notices() {
        return Email_Notices;
    }

    public void setEmail_Notices(ArrayList<String> email_Notices) {
        Email_Notices = email_Notices;
    }

    public ArrayList<String> getUI_Notices() {
        return UI_Notices;
    }

    public void setUI_Notices(ArrayList<String> UI_Notices) {
        this.UI_Notices = UI_Notices;
    }

    public void subscribeToChannel(YouTubeChannel youTubeChannel) {
        youTubeChannel.getNotifier().subscribeUser(this);
    }

    public void unsubscribeChannel(YouTubeChannel youTubeChannel) {
        youTubeChannel.getNotifier().unSubscribeUser(this);
    }
}
