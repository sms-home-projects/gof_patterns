package patterns.behavioral.Observer.observers;

import patterns.behavioral.Observer.users.YouTubeUser;

import java.util.ArrayList;
import java.util.List;

public class Notifier implements IObserver {

    private final List<YouTubeUser> users = new ArrayList<>();

    public void subscribeUser(YouTubeUser youTubeUser) {
        if (users.add(youTubeUser)) {
            System.out.println("Success subscribed");
        } else {
            System.out.println("Try again");
        }

    }

    public void unSubscribeUser(YouTubeUser youTubeUser) {
        users.remove(youTubeUser);
        System.out.println("Success unsubscribed");
    }

    @Override
    public void notify(String msg) {
        users.forEach(u -> u.update(msg));
    }
}
