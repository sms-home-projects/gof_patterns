package patterns.behavioral.Observer.observers;

public interface IObserver {
    void notify(String msg);
}
