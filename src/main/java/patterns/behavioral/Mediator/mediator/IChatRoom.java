package patterns.behavioral.Mediator.mediator;

import patterns.behavioral.Mediator.users.User;

public interface IChatRoom {
    void addUser(User user);

    void send(User user, String msg);
}
