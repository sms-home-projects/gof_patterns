package patterns.behavioral.Mediator.mediator;

import patterns.behavioral.Mediator.users.User;

import java.util.HashSet;
import java.util.Set;

public class ChatRoom implements IChatRoom {
    private final Set<User> users = new HashSet<>();
    private final StringBuffer text = new StringBuffer();

    @Override
    public void addUser(User user) {
        users.add(user);
        user.getChatRooms().add(this);
    }

    @Override
    public void send(User user, String msg) {
        text.append(user.getNickName() + ": " + msg + "\n");
    }

    public Set<User> getUsers() {
        return users;
    }


    public StringBuffer getText() {
        return text;
    }

    public void print() {
        System.out.println(getText());
    }
}
