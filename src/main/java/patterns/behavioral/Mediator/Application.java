package patterns.behavioral.Mediator;

import patterns.behavioral.Mediator.mediator.ChatRoom;
import patterns.behavioral.Mediator.users.User;

public class Application {
    public static void main(String[] args) {
        User user1 = new User("UserName");
        User user2 = new User("NickName");
        User user3 = new User("Login");

        ChatRoom chatRoom = new ChatRoom();
        ChatRoom chatRoom2 = new ChatRoom();

        chatRoom.addUser(user1);
        chatRoom.addUser(user2);
        chatRoom.addUser(user3);

        user1.sendMessage(chatRoom, "Hello");
        user2.sendMessage(chatRoom, "Hello guys!!");
        user3.sendMessage(chatRoom, "Hi!!!");

        System.out.println(chatRoom.getText());

        chatRoom2.addUser(user1);
        chatRoom2.addUser(user2);
        user1.sendMessage(chatRoom2, "What's up man?");
        user2.sendMessage(chatRoom2, "Yo!!!");

        user2.receiveMassages();
    }
}
