package patterns.behavioral.Mediator.users;

import patterns.behavioral.Mediator.mediator.ChatRoom;

import java.util.ArrayList;
import java.util.List;

public class User implements IUser {
    private String nickName;
    private List<ChatRoom> chatRooms;

    public User(String nickName) {
        this.nickName = nickName;
        this.chatRooms = new ArrayList<>();
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public List<ChatRoom> getChatRooms() {
        return chatRooms;
    }

    public void setChatRooms(List<ChatRoom> chatRooms) {
        this.chatRooms = chatRooms;
    }

    @Override
    public void sendMessage(ChatRoom chatRoom, String msg) {
        chatRoom.send(this, msg);
    }

    @Override
    public void receiveMassages() {
        chatRooms.forEach(c -> c.print());
    }
}
