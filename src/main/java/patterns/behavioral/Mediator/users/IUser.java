package patterns.behavioral.Mediator.users;

import patterns.behavioral.Mediator.mediator.ChatRoom;

public interface IUser {
    void sendMessage(ChatRoom chatRoom, String msg);

    void receiveMassages();
}
