package patterns.behavioral.Command.orders;

public interface IOrder {

    void execute();

}
