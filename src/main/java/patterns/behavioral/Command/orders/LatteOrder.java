package patterns.behavioral.Command.orders;

import patterns.behavioral.Command.stuff.Barista;

public class LatteOrder implements IOrder {
    private final Barista barista;

    public LatteOrder(Barista barista) {
        this.barista = barista;
    }

    @Override
    public void execute() {
        barista.makeLatte();
    }
}
