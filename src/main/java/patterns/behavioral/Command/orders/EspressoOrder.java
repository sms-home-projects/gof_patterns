package patterns.behavioral.Command.orders;

import patterns.behavioral.Command.stuff.Barista;

public class EspressoOrder implements IOrder {
    private  Barista barista;

    public EspressoOrder(Barista barista) {
        this.barista = barista;
    }

    @Override
    public void execute() {
        barista.makeEspresso();
    }
}
