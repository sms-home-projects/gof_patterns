package patterns.behavioral.Command.stuff;

//Receiver
public class Barista {

    public void makeEspresso() {
        System.out.println("Your Espresso is ready");
    }

    public void makeLatte() {
        System.out.println("Your Latte is ready");
    }
}
