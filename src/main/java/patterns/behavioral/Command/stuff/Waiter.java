package patterns.behavioral.Command.stuff;

import patterns.behavioral.Command.orders.IOrder;

//Invoker
public class Waiter {
    public void placeOrder(IOrder order) {
        order.execute();
    }
}
