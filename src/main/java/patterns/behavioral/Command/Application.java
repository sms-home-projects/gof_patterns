package patterns.behavioral.Command;

import patterns.behavioral.Command.orders.EspressoOrder;
import patterns.behavioral.Command.orders.LatteOrder;
import patterns.behavioral.Command.stuff.Barista;
import patterns.behavioral.Command.stuff.Waiter;

public class Application {
    public static void main(String[] args) {
        Barista barista = new Barista();
        Waiter waiter = new Waiter();

        waiter.placeOrder(new EspressoOrder(barista));
        waiter.placeOrder(new LatteOrder(barista));
    }
}
