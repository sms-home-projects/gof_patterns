package patterns.behavioral.Iterator.iterators;

import java.lang.reflect.Field;

public class ClassFieldsIterator implements Iterator {
    private Class clazz;
    private Field[] fields;
    private int pointer = -1;

    public ClassFieldsIterator() {
    }

    public ClassFieldsIterator(Class clazz) {
        this.clazz = clazz;
        this.fields = clazz.getDeclaredFields();
    }

    @Override
    public boolean hasNext() {
        return (pointer + 1 < fields.length);
    }

    @Override
    public Field next() {
        Field field = null;
        if (hasNext()) {
            pointer++;
            field = fields[pointer];
        }
        return field;
    }
}
