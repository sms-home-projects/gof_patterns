package patterns.behavioral.Iterator.iterators;

public interface Iterator<E> {

    boolean hasNext();

    E next();
}
