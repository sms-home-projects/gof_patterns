package patterns.behavioral.Iterator.iterators;

import java.lang.reflect.Method;

public class ClassMethodsIterator implements Iterator {
    private Class clazz;
    private Method[] methods;
    private int pointer = -1;

    public ClassMethodsIterator() {
    }

    public ClassMethodsIterator(Class clazz) {
        this.clazz = clazz;
        this.methods = clazz.getDeclaredMethods();
    }

    @Override
    public boolean hasNext() {
        return (pointer + 1 < methods.length);
    }

    @Override
    public Method next() {
        Method method = null;
        if (hasNext()) {
            pointer++;
            method = methods[pointer];
        }
        return method;
    }
}
