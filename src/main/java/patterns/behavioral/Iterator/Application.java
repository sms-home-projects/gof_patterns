package patterns.behavioral.Iterator;

import patterns.behavioral.Iterator.iterators.ClassFieldsIterator;
import patterns.behavioral.Iterator.iterators.ClassMethodsIterator;

public class Application {
    public static void main(String[] args) {

        ClassFieldsIterator classFieldsIterator = new ClassFieldsIterator(ClassFieldsIterator.class);
        while (classFieldsIterator.hasNext()) {
            System.out.println(classFieldsIterator.next());
        }

        ClassMethodsIterator classMethodsIterator = new ClassMethodsIterator(String.class);
        while (classMethodsIterator.hasNext()) {
            System.out.println(classMethodsIterator.next().getName() + "()");
        }
    }
}
