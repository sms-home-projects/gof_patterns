package patterns.behavioral.Template;

import patterns.behavioral.Template.events.Casual;
import patterns.behavioral.Template.events.PeterParker;
import patterns.behavioral.Template.events.SpiderMan;

public class Application {
    public static void main(String[] args) {

        PeterParker casual = new Casual();
        PeterParker spiderMan = new SpiderMan();

        casual.putOnAll();
        spiderMan.putOnAll();
    }
}
