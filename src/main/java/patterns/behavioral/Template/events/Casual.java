package patterns.behavioral.Template.events;

public class Casual extends PeterParker {
    @Override
    public void putOnShoes() {
        System.out.println("CASUAL SHOES +");
    }

    @Override
    public void putOnMask() {
        //doesn't need
    }

    @Override
    public void putOnTrousers() {
        System.out.println("CASUAL TROUSERS +");
    }

    @Override
    public void putOnShirt() {
        System.out.println("CASUAL SHIRT +");
    }
}
