package patterns.behavioral.Template.events;

public abstract class PeterParker {
    public void putOnUnderwear() {
        System.out.println("UNDERWEAR+");
    }

    public void putOnSocks() {
        System.out.println("SOCKS+");
    }

    public abstract void putOnShoes();

    public abstract void putOnMask();

    public abstract void putOnTrousers();

    public abstract void putOnShirt();

    public void putOnAll() {
        System.out.println("Peter Parker need " + this.getClass().getSimpleName() + " look");
        putOnUnderwear();
        putOnSocks();
        putOnShirt();
        putOnTrousers();
        putOnShoes();
        putOnMask();
        System.out.println(this.getClass().getSimpleName() + " look is complete");
    }
}
