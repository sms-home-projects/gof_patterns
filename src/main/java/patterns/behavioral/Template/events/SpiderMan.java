package patterns.behavioral.Template.events;

public class SpiderMan extends PeterParker {
    @Override
    public void putOnShoes() {
        System.out.println("SPIDERMAN SHOES+");
    }

    @Override
    public void putOnMask() {
        System.out.println("MASK+");
    }

    @Override
    public void putOnTrousers() {
        System.out.println("SPIDERMAN TROUSERS +");
    }

    @Override
    public void putOnShirt() {
        System.out.println("SPIDERMAN SHIRT +");
    }
}
