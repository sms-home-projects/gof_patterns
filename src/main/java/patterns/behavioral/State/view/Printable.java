package patterns.behavioral.State.view;

@FunctionalInterface
public interface Printable {

    void print();
}
