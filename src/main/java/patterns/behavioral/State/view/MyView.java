package patterns.behavioral.State.view;

import patterns.behavioral.State.Smartphone;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private final Map<String, String> menu;
    private final Map<String, Printable> methodsMenu;
    private static final Scanner input = new Scanner(System.in);
    private final Smartphone smartphone = new Smartphone();

    public MyView() {

        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Show state");
        menu.put("2", "  2 - Launch application");
        menu.put("3", "  3 - Show clock");
        menu.put("4", "  4 - Receive a call");
        menu.put("5", "  5 - Lock smartphone");
        menu.put("6", "  6 - Turn on ChildMode");
        menu.put("7", "  7 - Unlock smartphone");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
        methodsMenu.put("7", this::pressButton7);
    }

    private void pressButton1() {
        smartphone.showState();
    }

    private void pressButton2() {
        smartphone.launchApplication();
    }

    private void pressButton3() {
        smartphone.showClock();
    }

    private void pressButton4() {
        smartphone.receiveCall();
    }

    private void pressButton5() {
        smartphone.lock();

    }

    private void pressButton6() {
        smartphone.turnOnChildMode();
    }

    private void pressButton7() {
        smartphone.unlock();
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
