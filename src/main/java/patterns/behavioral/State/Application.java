package patterns.behavioral.State;

import patterns.behavioral.State.view.MyView;

public class Application {
    public static void main(String[] args) {
        new MyView().show();
    }
}
