package patterns.behavioral.State.states;

public class ChildMode extends SmartphoneState {
    @Override
    public void launchApplication() {
        System.out.println("Give smartphone to your parent");
    }

    @Override
    public void receiveCall() {
        System.out.println("Give smartphone to your parent");
    }


}
