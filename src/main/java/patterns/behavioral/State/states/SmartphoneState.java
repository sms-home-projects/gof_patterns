package patterns.behavioral.State.states;

import java.time.LocalTime;

public abstract class SmartphoneState {
    public abstract void launchApplication();

    public abstract void receiveCall();

    public void showState() {
        System.out.println("State: " + this.getClass().getSimpleName());
    }

    public void showClock() {
        System.out.println(LocalTime.now());
    }

}
