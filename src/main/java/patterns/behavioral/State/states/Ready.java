package patterns.behavioral.State.states;

public class Ready extends SmartphoneState {
    @Override
    public void launchApplication() {
        System.out.println("Success launched");
    }

    @Override
    public void receiveCall() {
        System.out.println("Enjoy your conversation");
    }
}
