package patterns.behavioral.State.states;

public class Locked extends SmartphoneState {
    @Override
    public void launchApplication() {
        System.out.println("Unlocked before!");
    }

    @Override
    public void receiveCall() {
        System.out.println("Success");
    }
}
