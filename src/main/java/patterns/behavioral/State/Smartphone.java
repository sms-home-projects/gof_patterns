package patterns.behavioral.State;

import patterns.behavioral.State.states.ChildMode;
import patterns.behavioral.State.states.Locked;
import patterns.behavioral.State.states.Ready;
import patterns.behavioral.State.states.SmartphoneState;

public class Smartphone {
    private String model;
    private SmartphoneState state = new Locked();

    public void lock() {
        setState(new Locked());
    }

    public void unlock() {
        setState(new Ready());
    }

    public void turnOnChildMode() {
        if (state.getClass().getSimpleName().equals("Ready")) {
            setState(new ChildMode());
        } else {
            System.out.println("Unlock before");
        }
    }

    public void launchApplication() {
        state.launchApplication();
    }

    public void receiveCall() {
        state.receiveCall();
    }

    public void showState() {
        state.showState();
    }

    public void showClock() {
        state.showClock();
    }

    public SmartphoneState getState() {
        return state;
    }

    public void setState(SmartphoneState state) {
        this.state = state;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
