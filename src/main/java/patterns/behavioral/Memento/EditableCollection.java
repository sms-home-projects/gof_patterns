package patterns.behavioral.Memento;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class EditableCollection<E> implements Collection {
    private final ArrayList<E> collection;
    private final Memento memento = new Memento();

    private class Memento {
        final int HISTORY_SIZE = 20;
        ArrayList<E>[] history = new ArrayList[HISTORY_SIZE];
        int pointer = 0;
        int last;

        Memento() {
            for (int i = 0; i < history.length; i++) {
                history[i] = new ArrayList<>();
            }
        }

        void save() {

            if (pointer == HISTORY_SIZE - 1) {
                for (int i = 0; i < HISTORY_SIZE - 1; i++) {
                    history[i] = history[i + 1];
                }
                history[HISTORY_SIZE - 1].addAll(collection);
            } else {
                history[pointer].addAll(collection);
                pointer++;
                last = pointer;
            }


        }

        public int getPointer() {
            return pointer;
        }

        public void setPointer(int pointer) {
            this.pointer = pointer;
        }

    }

    public void stepBack() {
        int p = memento.getPointer();
        if (p == 0) {
            System.out.println("You can't step back anymore");
        } else {
            memento.setPointer(p - 1);
            collection.clear();
            collection.addAll(memento.history[p - 2]);
        }
    }

    public void stepForward() {
        int p = memento.getPointer();
        if (p == memento.last) {
            System.out.println("You can't step forward anymore");
        } else {
            memento.setPointer(p + 1);
            collection.clear();
            collection.addAll(memento.history[p]);
        }
    }

    public void showFullHistory() {
        for (int i = 0; i < memento.history.length; i++) {
            System.out.println((i + 1) + " --> " + memento.history[i]);

        }
    }

    public EditableCollection(Collection collection) {
        this.collection = new ArrayList<>(collection);
    }

    @Override
    public int size() {
        return collection.size();
    }

    @Override
    public boolean isEmpty() {
        return collection.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return collection.contains(o);
    }

    @Override
    public Iterator iterator() {
        return collection.iterator();
    }

    @Override
    public void forEach(Consumer action) {
        collection.forEach(action);
    }

    @Override
    public Object[] toArray() {
        return collection.toArray();
    }

    @Override
    public boolean add(Object o) {
        boolean flag = false;
        if (collection.add((E) o)) {
            flag = true;
            memento.save();
        }
        return flag;
    }

    @Override
    public boolean remove(Object o) {
        boolean flag = false;
        if (collection.remove(o)) {
            flag = true;
            memento.save();
        }
        return flag;
    }

    @Override
    public boolean addAll(Collection c) {
        boolean flag = false;
        if (collection.addAll(c)) {
            flag = true;
            memento.save();
        }
        return flag;
    }

    @Override
    public boolean removeIf(Predicate filter) {
        boolean flag = false;
        if (collection.removeIf(filter)) {
            flag = true;
            memento.save();
        }
        return flag;
    }

    @Override
    public void clear() {
        collection.clear();
        memento.save();
    }

    @Override
    public Spliterator spliterator() {
        return collection.spliterator();
    }

    @Override
    public Stream stream() {
        return collection.stream();
    }

    @Override
    public Stream parallelStream() {
        return collection.parallelStream();
    }

    @Override
    public boolean retainAll(Collection c) {
        return collection.retainAll(c);
    }

    @Override
    public boolean removeAll(Collection c) {
        boolean flag = false;
        if (collection.removeAll(c)) {
            flag = true;
            memento.save();
        }
        return flag;
    }

    @Override
    public boolean containsAll(Collection c) {
        boolean flag = false;
        if (collection.containsAll(c)) {
            flag = true;
            memento.save();
        }
        return flag;
    }

    @Override
    public Object[] toArray(Object[] a) {
        return collection.toArray();
    }
}
