package patterns.behavioral.Memento;

import java.util.ArrayList;

public class Application {
    public static void main(String[] args) {

        ArrayList<String> arrayList = new ArrayList<>();
        EditableCollection editableCollection = new EditableCollection(arrayList);

        editableCollection.stepBack();
        editableCollection.forEach(System.out::println);

        editableCollection.add("First");
        editableCollection.add("Second");
        editableCollection.add("Third");

        editableCollection.remove("Second");
        editableCollection.forEach(System.out::println);

        editableCollection.stepBack();
        editableCollection.forEach(System.out::println);

        editableCollection.stepForward();
        editableCollection.forEach(System.out::println);

        editableCollection.stepForward();
        editableCollection.forEach(System.out::println);

        editableCollection.add("Fourth");
        editableCollection.clear();
        editableCollection.add("Fifth");

        editableCollection.showFullHistory();
    }
}
