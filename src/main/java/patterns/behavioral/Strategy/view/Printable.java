package patterns.behavioral.Strategy.view;

@FunctionalInterface
public interface Printable {

    void print();
}
