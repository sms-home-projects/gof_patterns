package patterns.behavioral.Strategy.view;

import patterns.behavioral.Strategy.stratagies.parallelograms.CalcRectangleArea;
import patterns.behavioral.Strategy.stratagies.parallelograms.CalcSquareAreaBySide;
import patterns.behavioral.Strategy.stratagies.triangels.CalcTriangleAreaBy2SidesAndAngleBetween;
import patterns.behavioral.Strategy.stratagies.triangels.CalcTriangleAreaBy3sides;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private final Map<String, String> menu;
    private final Map<String, Printable> methodsMenu;
    private static final Scanner input = new Scanner(System.in);

    public MyView() {

        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Calc triangle area by 3 sides");
        menu.put("2", "  2 - Calc triangle area by 2 sides and angle between them");
        menu.put("3", "  3 - Calc square area by side");
        menu.put("4", "  4 - Calc rectangle area by lengths of sides");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);

    }

    private void pressButton1() {
        System.out.println("Area -> " + new CalcTriangleAreaBy3sides().calc());
    }

    private void pressButton2() {
        System.out.println("Area -> " + new CalcTriangleAreaBy2SidesAndAngleBetween().calc());
    }

    private void pressButton3() {
        System.out.println("Area -> " + new CalcSquareAreaBySide().calc());
    }

    private void pressButton4() {
        System.out.println("Area -> " + new CalcRectangleArea().calc());
    }


    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
