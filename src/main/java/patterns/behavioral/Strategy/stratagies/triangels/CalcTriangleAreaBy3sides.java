package patterns.behavioral.Strategy.stratagies.triangels;

public class CalcTriangleAreaBy3sides extends CalcTriangleArea {
    private final String MSG = "Input please 3 lengths of sides";


    @Override
    public double calc(double a, double b, double c) {
        double p = (a + b + c) / 2;
        return Math.sqrt(p * (p - a) * (p - b) * (p - c));
    }

    @Override
    public String getMsg() {
        return MSG;
    }

}
