package patterns.behavioral.Strategy.stratagies.triangels;

public class CalcTriangleAreaBy2SidesAndAngleBetween extends CalcTriangleArea {
    private final String MSG = "Input please 2 lengths of sides and angle between in degrees";


    @Override
    public double calc(double a, double b, double c) {
        return 0.5 * a * b * Math.sin(Math.toRadians(c));
    }

    @Override
    public String getMsg() {
        return MSG;
    }

}
