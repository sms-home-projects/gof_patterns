package patterns.behavioral.Strategy.stratagies.triangels;

import patterns.behavioral.Strategy.stratagies.ICalcArea;

import java.util.Scanner;

public abstract class CalcTriangleArea implements ICalcArea {
    private double a;
    private double b;
    private double c;
    public String msg = "triangle";
    Scanner sc = new Scanner(System.in);

    @Override
    public double calc() {
        return calc(a, b, c);
    }

    public String getMsg() {
        return msg;
    }

    public CalcTriangleArea() {
        inputArgs();
    }

    public CalcTriangleArea(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public void inputArgs() {
        System.out.println(getMsg());
        a = sc.nextDouble();
        b = sc.nextDouble();
        c = sc.nextDouble();
    }

    public abstract double calc(double a, double b, double c);

}
