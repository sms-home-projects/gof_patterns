package patterns.behavioral.Strategy.stratagies;

public interface ICalcArea {
    double calc();
}
