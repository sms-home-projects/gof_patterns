package patterns.behavioral.Strategy.stratagies.parallelograms;

import patterns.behavioral.Strategy.stratagies.ICalcArea;

import java.util.Scanner;

public class CalcRectangleArea implements ICalcArea {
    private final double a;
    private final double b;
    private final String MSG = "Input please lengths of sides ";
    private final Scanner sc = new Scanner(System.in);

    public CalcRectangleArea() {
        System.out.println(MSG);
        a = sc.nextDouble();
        b = sc.nextDouble();
    }

    @Override
    public double calc() {
        return a * b;
    }
}
