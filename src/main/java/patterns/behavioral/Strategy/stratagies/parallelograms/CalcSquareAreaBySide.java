package patterns.behavioral.Strategy.stratagies.parallelograms;

import patterns.behavioral.Strategy.stratagies.ICalcArea;

import java.util.Scanner;

public class CalcSquareAreaBySide implements ICalcArea {
    private final double a;
    private final String MSG = "Input side length please ";
    private final Scanner sc = new Scanner(System.in);

    public CalcSquareAreaBySide() {
        System.out.println(MSG);
        a = sc.nextDouble();
    }

    @Override
    public double calc() {
        return a * a;
    }
}
