package patterns.behavioral.Strategy;


import patterns.behavioral.Strategy.view.MyView;

public class Application {
    public static void main(String[] args) {
        new MyView().show();
    }
}
