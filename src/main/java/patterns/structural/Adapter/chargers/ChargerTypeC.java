package patterns.structural.Adapter.chargers;

import patterns.structural.Adapter.Port;

public class ChargerTypeC implements Charger {
    private Port port;


    public ChargerTypeC() {
        port = Port.TYPE_C;
    }

    public Port getPort() {
        return port;
    }
}
