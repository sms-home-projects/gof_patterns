package patterns.structural.Adapter.chargers;

import patterns.structural.Adapter.Port;

public interface Charger {
    Port getPort();
}
