package patterns.structural.Adapter.chargers;

import patterns.structural.Adapter.Port;

public class ChargerMiniUSB implements Charger {
    private Port port;


    public ChargerMiniUSB() {
        port = Port.MINI_USB;
    }

    public Port getPort() {
        return port;
    }
}
