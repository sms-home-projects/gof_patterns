package patterns.structural.Adapter;

public enum Port {
    MINI_USB, TYPE_C
}
