package patterns.structural.Adapter.adapters;

import patterns.structural.Adapter.Port;
import patterns.structural.Adapter.chargers.Charger;

public class UniversalAdapter implements Charger {
    private Port port;


    public UniversalAdapter(Charger charger) {
        port = (charger.getPort().equals(Port.TYPE_C)) ? Port.MINI_USB : Port.TYPE_C;

    }

    @Override
    public Port getPort() {
        return port;
    }
}
