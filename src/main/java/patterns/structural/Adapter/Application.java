package patterns.structural.Adapter;

import patterns.structural.Adapter.adapters.UniversalAdapter;
import patterns.structural.Adapter.chargers.Charger;
import patterns.structural.Adapter.chargers.ChargerMiniUSB;
import patterns.structural.Adapter.chargers.ChargerTypeC;
import patterns.structural.Adapter.smartphones.Smartphone;

public class Application {

    public static void main(String[] args) throws InterruptedException {
        Smartphone iphone = new Smartphone("12", 75, Port.TYPE_C);
        Smartphone samsung = new Smartphone("S10", 84, Port.MINI_USB);


        Charger miniUSB = new ChargerMiniUSB();
        Charger typeC = new ChargerTypeC();

        iphone.charge(miniUSB);     // This charger is incompatible
        iphone.charge(typeC);       // OK
        samsung.charge(typeC);      // This charger is incompatible
        samsung.charge(miniUSB);    // OK

        iphone.setBatteryLevel(94);
        samsung.setBatteryLevel(95);

        iphone.charge(new UniversalAdapter(miniUSB)); // OK
        samsung.charge(new UniversalAdapter(typeC));  // OK
    }
}
