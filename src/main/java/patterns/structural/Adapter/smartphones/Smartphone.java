package patterns.structural.Adapter.smartphones;

import patterns.structural.Adapter.Port;
import patterns.structural.Adapter.chargers.Charger;

import java.util.concurrent.TimeUnit;

public class Smartphone {
    private String model;
    private int batteryLevel;
    private Port port;

    public Smartphone() {
    }

    public Smartphone(String model) {
        this.model = model;
    }

    public Smartphone(String model, int batteryLevel, Port port) {
        this.model = model;
        if (batteryLevel < 0) batteryLevel = 0;
        if (batteryLevel > 100) batteryLevel = 100;
        this.batteryLevel = batteryLevel;
        this.port = port;
    }


    public String getModel() {
        return model;
    }

    public int getBatteryLevel() {
        return batteryLevel;
    }

    public Port getPort() {
        return port;
    }

    public void setBatteryLevel(int batteryLevel) {
        if (batteryLevel < 0) batteryLevel = 0;
        if (batteryLevel > 100) batteryLevel = 100;
        this.batteryLevel = batteryLevel;
    }

    public void charge(Charger charger) throws InterruptedException {
        if (!this.port.equals(charger.getPort())) {
            System.out.println("This charger is incompatible");
        } else {
            System.out.println("This charger is compatible");
            while (batteryLevel < 100) {
                System.out.println("Battery level " + batteryLevel++);
                TimeUnit.SECONDS.sleep(1);
            }
            System.out.println("Charge completed");
        }


    }
}
