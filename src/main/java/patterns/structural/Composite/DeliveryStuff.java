package patterns.structural.Composite;

public interface DeliveryStuff {
    double getPrice();
}
