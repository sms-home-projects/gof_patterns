package patterns.structural.Composite;

import java.util.ArrayList;

public class DeliveryBox implements DeliveryStuff {
    private ArrayList<DeliveryStuff> deliveryStuffs = new ArrayList<>();

    void put(DeliveryStuff deliveryStuff) {
        deliveryStuffs.add(deliveryStuff);
    }

    ;

    void remove(DeliveryStuff deliveryStuff) {
        deliveryStuffs.remove(deliveryStuff);
    }

    ;

    public ArrayList<DeliveryStuff> getDeliveryStuffs() {
        return deliveryStuffs;
    }

    public void setDeliveryStuffs(ArrayList<DeliveryStuff> deliveryStuffs) {
        this.deliveryStuffs = deliveryStuffs;
    }

    @Override
    public double getPrice() {
        double sum = 0;
        for (DeliveryStuff deliveryStuff : deliveryStuffs) {
            sum += deliveryStuff.getPrice();
        }
        return sum;
    }

    @Override
    public String toString() {
        return "DeliveryBox{" +
                "deliveryStuffs=" + deliveryStuffs +
                '}';
    }
}
