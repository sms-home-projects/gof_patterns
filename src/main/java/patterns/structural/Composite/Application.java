package patterns.structural.Composite;

public class Application {
    public static void main(String[] args) {
        Stuff hummer = new Stuff();
        hummer.setName("h001");
        hummer.setDescription("Can be danger for your fingers");
        hummer.setPrice(50);
        Stuff calc = new Stuff("EngineerCalc", 150, "The best of the best");
        Stuff noteBook = new Stuff("NB2030", 20, "");

        DeliveryBox box = new DeliveryBox();
        DeliveryBox bigBox = new DeliveryBox();

        box.put(calc);
        box.put(noteBook);

        bigBox.put(box);
        bigBox.put(hummer);

        System.out.println("All is packed to 1 box. Total price - " + bigBox.getPrice());

    }
}
