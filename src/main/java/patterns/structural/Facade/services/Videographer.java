package patterns.structural.Facade.services;

public class Videographer extends AbstractGrapher {


    public Videographer(String name, int pricePerHour, boolean isFree) {
        super(name, pricePerHour, isFree);
    }

    public void useDrone(int addPrice) {
        this.pricePerHour += addPrice;
    }
}
