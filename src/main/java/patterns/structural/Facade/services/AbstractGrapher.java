package patterns.structural.Facade.services;

public class AbstractGrapher {
    private String name;
    protected int pricePerHour;
    private boolean isFree;

    public int hireMe(int hours) {
        int totalPrice = 0;
        if (isFree) {
            isFree = false;
            totalPrice = pricePerHour * hours;
        }
        return totalPrice;
    }

    public AbstractGrapher(String name, int pricePerHour, boolean isFree) {
        this.name = name;
        this.pricePerHour = pricePerHour;
        this.isFree = isFree;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPricePerHour() {
        return pricePerHour;
    }

    public void setPricePerHour(int pricePerHour) {
        this.pricePerHour = pricePerHour;
    }

    public boolean isFree() {
        return isFree;
    }

    public void setFree(boolean free) {
        isFree = free;
    }
}
