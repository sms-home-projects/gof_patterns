package patterns.structural.Facade.services;

public class Photographer extends AbstractGrapher {


    public Photographer(String name, int pricePerHour, boolean isFree) {
        super(name, pricePerHour, isFree);
    }

    public void createAlbum() {
        System.out.println("Your album is ready");
    }
}
