package patterns.structural.Facade.services;

public class HallDecorator {
    public void showCatalog() {
        System.out.println("Decorator: Catalog is shown");
    }

    ;

    public void decorate() {
        System.out.println("Decorator: The hall is decorated");
    }

    public int takeMoney(int price) {
        System.out.println("Decorator: Pay pls " + price);
        return price;
    }
}
