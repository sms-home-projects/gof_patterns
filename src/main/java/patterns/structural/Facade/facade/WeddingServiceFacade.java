package patterns.structural.Facade.facade;

import patterns.structural.Facade.services.HallDecorator;
import patterns.structural.Facade.services.Photographer;
import patterns.structural.Facade.services.Videographer;

import java.util.ArrayList;

public class WeddingServiceFacade {
    private ArrayList<Photographer> photographers = new ArrayList<>();
    private ArrayList<Videographer> videographers = new ArrayList<>();
    private HallDecorator hallDecorator = new HallDecorator();

    private int totalPrice = 0;

    public Photographer hirePhotographer(int hours) {
        Photographer freePhotographer =
                photographers.stream()
                        .filter(p -> p.isFree())
                        .findFirst()
                        .get();
        int price = freePhotographer.hireMe(hours);
        this.totalPrice += price;
        System.out.println("Photographer " + freePhotographer.getName() + "  + " + price + "USD");
        return freePhotographer;
    }

    public Videographer hireVideographer(int hours) {
        Videographer freeVideographer =
                videographers.stream()
                        .filter(v -> v.isFree())
                        .findFirst()
                        .get();
        int price = freeVideographer.hireMe(hours);
        this.totalPrice += price;
        System.out.println("Videographer " + freeVideographer.getName() + "  + " + price + "USD");
        return freeVideographer;
    }

    public void decorate() {
        hallDecorator.showCatalog();
        hallDecorator.decorate();
        totalPrice += hallDecorator.takeMoney(150);
    }

    public ArrayList<Photographer> getPhotographers() {
        return photographers;
    }

    public void setPhotographers(ArrayList<Photographer> photographers) {
        this.photographers = photographers;
    }

    public ArrayList<Videographer> getVideographers() {
        return videographers;
    }

    public void setVideographers(ArrayList<Videographer> videographers) {
        this.videographers = videographers;
    }

    public HallDecorator getHallDecorator() {
        return hallDecorator;
    }

    public void setHallDecorator(HallDecorator hallDecorator) {
        this.hallDecorator = hallDecorator;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }
}

