package patterns.structural.Facade;

import patterns.structural.Facade.facade.WeddingServiceFacade;
import patterns.structural.Facade.services.Photographer;
import patterns.structural.Facade.services.Videographer;

import java.util.ArrayList;

public class Application {
    public static void main(String[] args) {

        WeddingServiceFacade weddingService = new WeddingServiceFacade();

        ArrayList<Photographer> photographers = new ArrayList<>();
        ArrayList<Videographer> videographers = new ArrayList<>();

        photographers.add(new Photographer("Petro", 10, false));
        photographers.add(new Photographer("Ivan", 12, true));
        photographers.add(new Photographer("Anna", 8, true));

        videographers.add(new Videographer("Stepan", 14, false));
        videographers.add(new Videographer("Andrii", 15, false));
        videographers.add(new Videographer("Olha", 15, true));

        weddingService.setPhotographers(photographers);
        weddingService.setVideographers(videographers);

        weddingService.decorate();
        weddingService.hirePhotographer(15);
        weddingService.hireVideographer(20);
        System.out.println("Total: " + weddingService.getTotalPrice() + "USD");
    }
}
