package patterns.structural.Decorator.decorator;

public interface BouquetFace {

    double getTotalPrice();

    void print();
}
