package patterns.structural.Decorator.decorator;


import java.util.Optional;

public class BouquetDecorator implements BouquetFace {
    private Optional<BouquetFace> bouquet;
    private double additionalPrice;
    private String additionalComponent;

    public void setBouquet(BouquetFace bouquet) {
        this.bouquet = Optional.ofNullable(bouquet);
    }

    public void setAdditionalPrice(double additionalPrice) {
        this.additionalPrice = additionalPrice;
    }

    public void setAdditionalComponent(String additionalComponent) {
        this.additionalComponent = additionalComponent;
    }

    @Override
    public double getTotalPrice() {
        return bouquet.orElseThrow(IllegalArgumentException::new).getTotalPrice() + additionalPrice;
    }

    @Override
    public void print() {
        bouquet.get().print();
        System.out.println(additionalComponent + "\t" + additionalPrice);
    }
}
