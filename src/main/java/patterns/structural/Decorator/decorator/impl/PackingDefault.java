package patterns.structural.Decorator.decorator.impl;


import patterns.structural.Decorator.decorator.BouquetDecorator;

public class PackingDefault extends BouquetDecorator {
    private final double ADDITIONAL_PRICE = 20;
    private final String ADDITIONAL_COMPONENT = "+ Default packing";

    public PackingDefault() {
        setAdditionalPrice(ADDITIONAL_PRICE);
        setAdditionalComponent(ADDITIONAL_COMPONENT);
    }
}

