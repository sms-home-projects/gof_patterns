package patterns.structural.Decorator.decorator.impl;


import patterns.structural.Decorator.decorator.BouquetDecorator;
import patterns.structural.Decorator.domain.Bouquet;

public class Discount extends BouquetDecorator {
    private double ADDITIONAL_PRICE;
    private String ADDITIONAL_COMPONENT;

    public Discount(Bouquet bouquet, Cards card) {
        ADDITIONAL_COMPONENT = "Used discount of " + card;

        ADDITIONAL_PRICE = bouquet.getTotalPrice() * 0.05;

        setAdditionalPrice(-ADDITIONAL_PRICE);
        setAdditionalComponent(ADDITIONAL_COMPONENT);
    }

    public Discount(Cards card) {
        ADDITIONAL_COMPONENT = "Used discount of " + card;
        switch (card) {
            case Gold: {
                ADDITIONAL_PRICE = 50;
                break;
            }
            case Social: {
                ADDITIONAL_PRICE = 30;
                break;
            }
        }
        setAdditionalPrice(-ADDITIONAL_PRICE);
        setAdditionalComponent(ADDITIONAL_COMPONENT);
    }
}
