package patterns.structural.Decorator;


import patterns.structural.Decorator.decorator.BouquetDecorator;
import patterns.structural.Decorator.decorator.impl.*;
import patterns.structural.Decorator.domain.*;

import java.util.HashMap;
import java.util.Map;

public class Application {
    public static void main(String[] args) {
        FlowerCatalog flowerCatalog = new FlowerCatalog();
        flowerCatalog.addFlower(Flower.Tulip, 24.5);
        flowerCatalog.addFlower(Flower.Rose, 70.0);
        flowerCatalog.addFlower(Flower.Daisy, 12.3);
        flowerCatalog.addFlower(Flower.Lily, 35.3);
        flowerCatalog.addFlower(Flower.Orchid, 100.0);
        flowerCatalog.printCatalog();

        Bouquet bouquet1 = new Bouquet();
        bouquet1.setName("Morning");
        Map<Flower, Integer> flowers = new HashMap<>();
        flowers.put(Flower.Daisy, 21);
        bouquet1.setFlowers(flowers);
        bouquet1.setBouquetType(BouquetType.VALENTINE);

        Bouquet bouquet2 = new Bouquet();
        bouquet2.setName("Happiness");
        Map<Flower, Integer> flowers2 = new HashMap<>();
        flowers2.put(Flower.Daisy, 13);
        flowers2.put(Flower.Rose, 9);
        flowers2.put(Flower.Orchid, 3);
        bouquet2.setFlowers(flowers2);
        bouquet2.setBouquetType(BouquetType.BIRTHDAY);

        FlowerShop flowerShop = new FlowerShop();
        flowerShop.addNewBouquet(bouquet1);
        flowerShop.addNewBouquet(bouquet2);
        flowerShop.setFlowerCatalog(flowerCatalog);


        BouquetDecorator packingCool = new PackingCool();
        BouquetDecorator packingDefault = new PackingDefault();
        BouquetDecorator discountGold = new Discount(Cards.Gold);

        BouquetDecorator delivery = new Delivery();

        packingDefault.setBouquet(bouquet1);
        delivery.setBouquet(packingDefault);
        discountGold.setBouquet(delivery);
        BouquetDecorator bouquetDecorator1 = discountGold;
        bouquetDecorator1.print();
        System.out.println("Total price - " + bouquetDecorator1.getTotalPrice());

        System.out.println("Now you can create your own bouquet");
        Bouquet customerBouquet = flowerShop.createUniqBouquetForCustomer();

        packingCool.setBouquet(customerBouquet);
        delivery.setBouquet(packingCool);
        BouquetDecorator discountBonus = new Discount(customerBouquet, Cards.Bonus);
        discountBonus.setBouquet(delivery);

        BouquetDecorator finalCustomerBouquet = discountBonus;
        finalCustomerBouquet.print();
        System.out.println("Total price -> " + finalCustomerBouquet.getTotalPrice());
    }
}
