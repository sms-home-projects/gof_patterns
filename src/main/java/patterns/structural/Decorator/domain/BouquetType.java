package patterns.structural.Decorator.domain;

public enum BouquetType {
    WEDDING, BIRTHDAY, VALENTINE, FUNERAL, NONE
}
