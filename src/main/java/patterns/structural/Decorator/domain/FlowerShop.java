package patterns.structural.Decorator.domain;

import java.util.*;

public class FlowerShop {
    private FlowerCatalog flowerCatalog;
    private List<Bouquet> bouquets = new ArrayList<Bouquet>();
    private Scanner sc = new Scanner(System.in);

    public FlowerCatalog getFlowerCatalog() {
        return flowerCatalog;
    }

    public void setFlowerCatalog(FlowerCatalog flowerCatalog) {
        this.flowerCatalog = flowerCatalog;
        if (!bouquets.isEmpty()) {
            Double newPrice;
            for (Bouquet bouquet : bouquets) {
                newPrice = 0.0;
                for (Map.Entry<Flower, Integer> entry : bouquet.getFlowers().entrySet()) {
                    newPrice += entry.getValue() * flowerCatalog.getFlowerPrice(entry.getKey());
                }
                bouquet.setPrice(newPrice);
            }
        }
        ;
    }

    public void addNewBouquet(Bouquet bouquet) {
        bouquets.add(bouquet);
        if (flowerCatalog != null) setFlowerCatalog(flowerCatalog);
    }

    public Bouquet createUniqBouquetForCustomer() {
        Bouquet newBouquet = new Bouquet();
        System.out.println("Your bouquet name?");
        newBouquet.setName(sc.next());
        newBouquet.setPrice(0.0);
        boolean q = true;
        do {
            Set<Map.Entry<Flower, Double>> entries = flowerCatalog.getPrices().entrySet();
            Iterator<Map.Entry<Flower, Double>> iterator = entries.iterator();
            int i = 0;
            Map<Integer, Map.Entry<Flower, Double>> numberEntry = new HashMap<>();
            while (iterator.hasNext()) {
                Map.Entry entry = iterator.next();
                numberEntry.put(i, entry);
                System.out.println("Input " + (i++) + " to add " + entry.getKey() + " ---- " + entry.getValue() + "uah");
            }
            System.out.println("  Q  -  To finish adding flowers");


            try {
                q = sc.nextLine().equalsIgnoreCase("Q");
                if (!q) {
                    int flowerIndex = sc.nextInt();
                    Flower flower = numberEntry.get(flowerIndex).getKey();
                    Double value = numberEntry.get(flowerIndex).getValue();
                    System.out.println("What the number of " + flower + " would you like to add to your " + newBouquet.getName() + " bouquet?");
                    int count = sc.nextInt();
                    if (newBouquet.getFlowers().containsKey(flower)) {
                        newBouquet.setPrice(newBouquet.getTotalPrice() - newBouquet.getFlowers().get(flower) * value);
                        count += newBouquet.getFlowers().get(flower);
                    }
                    newBouquet.getFlowers().put(flower, count);
                    newBouquet.setPrice(newBouquet.getTotalPrice() + count * value);
                    newBouquet.print();
                }
            } catch (Exception e) {
                e.printStackTrace();
                System.err.println("Input correct data!!!");
            }

        } while (q);

        BouquetType[] bouquetTypes = BouquetType.values();
        for (int j = 0; j < bouquetTypes.length; j++) {
            System.out.println("Input " + j + " to set type " + bouquetTypes[j]);
        }
        int input = sc.nextInt();
        if (input < bouquetTypes.length & input >= 0) {
            newBouquet.setBouquetType(bouquetTypes[input]);
        } else newBouquet.setBouquetType(BouquetType.NONE);

        return newBouquet;
    }

    public List<Bouquet> getBouquets() {
        return bouquets;
    }

    public void setBouquets(List<Bouquet> bouquets) {
        this.bouquets = bouquets;
    }


}
