package patterns.structural.Decorator.domain;


import patterns.structural.Decorator.decorator.BouquetFace;

import java.util.HashMap;
import java.util.Map;

public class Bouquet implements BouquetFace {
    private String name;
    private Map<Flower, Integer> flowers = new HashMap<Flower, Integer>();
    private BouquetType bouquetType;
    private Double price;

    public Bouquet() {
    }

    public Bouquet(String name, Map<Flower, Integer> flowers, BouquetType bouquetType) {
        this.name = name;
        this.flowers = flowers;
        this.bouquetType = bouquetType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<Flower, Integer> getFlowers() {
        return flowers;
    }

    public void setFlowers(Map<Flower, Integer> flowers) {
        this.flowers = flowers;
    }

    public BouquetType getBouquetType() {
        return bouquetType;
    }

    public void setBouquetType(BouquetType bouquetType) {
        this.bouquetType = bouquetType;
    }


    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public double getTotalPrice() {
        return price;
    }

    public void print() {
        System.out.println("Bouquet " + name + " for " + bouquetType + " include such flowers: ");
        for (Map.Entry<Flower, Integer> entry : flowers.entrySet()) {
            System.out.println(entry.getKey() + "      <--->      " + entry.getValue());
        }
        System.out.println("Bouquet costs " + price + "uah");
    }
}
