package patterns.structural.Proxy.warriors;

public interface Warrior {
    int moveForward(int steps);

    int moveLeft(int steps);

    int moveRight(int steps);

    int moveBack(int steps);

    int attack();
}
