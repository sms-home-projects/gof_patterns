package patterns.structural.Proxy.warriors;

public class Soldier implements Warrior {
    private int healthLevel;
    private int attackStrength;

    public Soldier() {
    }

    public Soldier(int healthLevel, int attackStrength) {
        this.healthLevel = healthLevel;
        this.attackStrength = attackStrength;
    }

    @Override
    public int moveForward(int steps) {
        System.out.println("Replaced forward " + steps + " steps");
        return steps;
    }

    @Override
    public int moveLeft(int steps) {
        System.out.println("Replaced left " + steps + " steps");
        return steps;
    }

    @Override
    public int moveRight(int steps) {
        System.out.println("Replaced right " + steps + " steps");
        return steps;
    }

    @Override
    public int moveBack(int steps) {
        System.out.println("Replaced back " + steps + " steps");
        return steps;
    }

    @Override
    public int attack() {
        System.out.println("Soldier made damage " + attackStrength);
        return attackStrength;
    }

    public int getHealthLevel() {
        return healthLevel;
    }

    public void setHealthLevel(int healthLevel) {
        this.healthLevel = healthLevel;
    }

    public int getAttackStrength() {
        return attackStrength;
    }

    public void setAttackStrength(int attackStrength) {
        this.attackStrength = attackStrength;
    }
}
