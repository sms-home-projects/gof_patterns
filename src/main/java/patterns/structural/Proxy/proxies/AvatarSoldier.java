package patterns.structural.Proxy.proxies;

import patterns.structural.Proxy.warriors.Soldier;
import patterns.structural.Proxy.warriors.Warrior;

public class AvatarSoldier implements Warrior {
    private Soldier soldier;
    private int multiplierSteps;
    private int multiplayerAttack;

    public AvatarSoldier() {
    }

    public AvatarSoldier(int multiplierSteps, int multiplayerAttack) {
        this.multiplierSteps = multiplierSteps;
        this.multiplayerAttack = multiplayerAttack;
    }

    @Override
    public int moveForward(int steps) {
        steps *= multiplierSteps;
        System.out.println("Replaced forward " + steps + " steps");
        return steps;
    }

    @Override
    public int moveLeft(int steps) {
        steps *= multiplierSteps;
        System.out.println("Replaced left " + steps + " steps");
        return steps;
    }

    @Override
    public int moveRight(int steps) {
        steps *= multiplierSteps;
        System.out.println("Replaced right " + steps + " steps");
        return steps;
    }

    @Override
    public int moveBack(int steps) {
        steps *= multiplierSteps;
        System.out.println("Replaced back " + steps + " steps");
        return steps;
    }

    @Override
    public int attack() {
        int attack = soldier.attack() * multiplayerAttack;
        System.out.println("Soldier made damage " + attack);
        return attack;
    }

    public Soldier getSoldier() {
        return soldier;
    }

    public void setSoldier(Soldier soldier) {
        this.soldier = soldier;
    }

    public int getMultiplierSteps() {
        return multiplierSteps;
    }

    public void setMultiplierSteps(int multiplierSteps) {
        this.multiplierSteps = multiplierSteps;
    }

    public int getMultiplayerAttack() {
        return multiplayerAttack;
    }

    public void setMultiplayerAttack(int multiplayerAttack) {
        this.multiplayerAttack = multiplayerAttack;
    }
}
