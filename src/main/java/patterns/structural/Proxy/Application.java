package patterns.structural.Proxy;

import patterns.structural.Proxy.proxies.AvatarSoldier;
import patterns.structural.Proxy.warriors.Soldier;
import patterns.structural.Proxy.warriors.Warrior;

public class Application {

    public static void runFightStrategy(Warrior warrior) {
        warrior.moveLeft(2);
        warrior.attack();
        warrior.moveForward(3);
        warrior.moveRight(4);
        warrior.attack();
        warrior.moveBack(1);
        warrior.attack();
    }

    public static void main(String[] args) {
        Soldier soldier = new Soldier(230, 50);
        runFightStrategy(soldier);

        AvatarSoldier avatar = new AvatarSoldier(2, 15);
        avatar.setSoldier(soldier);
        runFightStrategy(avatar);
    }
}
