package patterns.structural.Bridge.views;

import patterns.structural.Bridge.products.Product;

public class FullView extends AbstractView {

    public FullView(Product product) {
        super(product);
    }

    @Override
    public void show() {
        System.out.println(this.product.showName());
        System.out.println(this.product.linkToProductOwner());
        System.out.println(this.product.showDescription());
    }


}
