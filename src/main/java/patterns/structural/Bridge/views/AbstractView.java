package patterns.structural.Bridge.views;

import patterns.structural.Bridge.products.Product;

public abstract class AbstractView {
    Product product;

    public AbstractView(Product product) {
        this.product = product;
    }

    public void show() {
    }

    ;

    public void buy() {
        System.out.println("Please pay " + this.product.getPrice());
    }
}
