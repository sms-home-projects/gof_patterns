package patterns.structural.Bridge.views;

import patterns.structural.Bridge.products.Product;

public class CompactView extends AbstractView {

    public CompactView(Product product) {
        super(product);
    }

    @Override
    public void show() {
        System.out.println(this.product.showName());
        System.out.println(this.product.linkToProductOwner());
    }


}
