package patterns.structural.Bridge;

import patterns.structural.Bridge.products.Book;
import patterns.structural.Bridge.products.NoteBook;
import patterns.structural.Bridge.views.CompactView;
import patterns.structural.Bridge.views.FullView;

public class Application {
    public static void main(String[] args) {
        Book book = new Book("Name", "Author", "This boook is about...", "http://owner.com", 220.00);
        NoteBook noteBook = new NoteBook("NB0001", 120, 20, 15, "N/A", 2.5);

        CompactView cVBook = new CompactView(book);
        CompactView cVNoteBook = new CompactView(noteBook);

        FullView fVBook = new FullView(book);
        FullView fVNoteBook = new FullView(noteBook);

        cVBook.show();
        fVBook.show();
        fVBook.buy();

        cVNoteBook.show();
        fVNoteBook.show();
        cVNoteBook.buy();


    }
}
