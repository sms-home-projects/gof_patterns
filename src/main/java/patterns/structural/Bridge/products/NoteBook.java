package patterns.structural.Bridge.products;

public class NoteBook implements Product {
    private String model;
    private int numberOfPages;
    private int length;
    private int width;
    private String productOwner;
    private double price;

    public NoteBook() {
    }

    public NoteBook(String model, int numberOfPages, int length, int width, String productOwner, double price) {
        this.model = model;
        this.numberOfPages = numberOfPages;
        this.length = length;
        this.width = width;
        this.productOwner = productOwner;
        this.price = price;
    }

    @Override
    public String showName() {
        return model;
    }

    @Override
    public String showDescription() {
        String description = "Number of pages " + numberOfPages +
                ". Size: " + length + "x" + width;
        return description;
    }

    @Override
    public String linkToProductOwner() {
        return productOwner;
    }

    @Override
    public double getPrice() {
        return price;
    }
}
