package patterns.structural.Bridge.products;

public interface Product {
    String showName();

    String showDescription();

    String linkToProductOwner();

    double getPrice();
}
