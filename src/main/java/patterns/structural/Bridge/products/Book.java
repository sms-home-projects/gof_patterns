package patterns.structural.Bridge.products;

public class Book implements Product {
    private String name;
    private String author;
    private String about;
    private String productOwner;
    private double price;

    public Book() {
    }

    public Book(String name, String author, String about, String productOwner, double price) {
        this.name = name;
        this.author = author;
        this.about = about;
        this.productOwner = productOwner;
        this.price = price;
    }

    @Override
    public String showName() {
        return name;
    }

    @Override
    public String showDescription() {
        return author + " " + about;
    }

    @Override
    public String linkToProductOwner() {
        return productOwner;
    }

    @Override
    public double getPrice() {
        return price;
    }
}
