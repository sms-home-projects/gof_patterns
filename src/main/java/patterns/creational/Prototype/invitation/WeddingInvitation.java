package patterns.creational.Prototype.invitation;

public class WeddingInvitation extends AbstractInvitation {
    private String guestNamePartner;

    public WeddingInvitation() {

    }

    private WeddingInvitation(WeddingInvitation weddingInvitation) {
        super(weddingInvitation);
        if (weddingInvitation != null) {
            this.guestNamePartner = weddingInvitation.guestNamePartner;
        }
    }

    @Override
    public WeddingInvitation clone() {
        return new WeddingInvitation(this);
    }

    public void setGuestNamePartner(String guestNamePartner) {
        this.guestNamePartner = guestNamePartner;
    }

    @Override
    public String toString() {
        return "WeddingInvitation{" +
                "Dear, " + guestName + " and " + guestNamePartner +
                " " + text + " at " + date +
                '}';
    }
}
