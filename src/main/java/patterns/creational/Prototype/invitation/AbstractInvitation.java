package patterns.creational.Prototype.invitation;

import java.util.Date;

public abstract class AbstractInvitation {
    public String guestName;
    public String text;
    public Date date;

    public AbstractInvitation() {
    }

    public AbstractInvitation(AbstractInvitation abstractInvitation) {
        if (abstractInvitation != null) {
            this.guestName = abstractInvitation.guestName;
            this.text = abstractInvitation.text;
            this.date = abstractInvitation.date;
        }
    }

    public abstract AbstractInvitation clone();

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractInvitation)) return false;

        AbstractInvitation that = (AbstractInvitation) o;

        if (getGuestName() != null ? !getGuestName().equals(that.getGuestName()) : that.getGuestName() != null)
            return false;
        if (getText() != null ? !getText().equals(that.getText()) : that.getText() != null) return false;
        return getDate() != null ? getDate().equals(that.getDate()) : that.getDate() == null;
    }

    @Override
    public int hashCode() {
        int result = getGuestName() != null ? getGuestName().hashCode() : 0;
        result = 31 * result + (getText() != null ? getText().hashCode() : 0);
        result = 31 * result + (getDate() != null ? getDate().hashCode() : 0);
        return result;
    }

}
