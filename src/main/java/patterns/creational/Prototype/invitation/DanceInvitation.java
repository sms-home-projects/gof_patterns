package patterns.creational.Prototype.invitation;

public class DanceInvitation extends AbstractInvitation {
    private String color;

    public DanceInvitation() {
    }

    private DanceInvitation(DanceInvitation danceInvitation) {
        super(danceInvitation);
        if (danceInvitation != null) {
            this.color = danceInvitation.color;
        }
    }

    @Override
    public AbstractInvitation clone() {
        return new DanceInvitation(this);
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }
}
