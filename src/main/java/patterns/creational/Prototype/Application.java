package patterns.creational.Prototype;

import patterns.creational.Prototype.invitation.WeddingInvitation;

import java.util.*;

public class Application {
    public static void main(String[] args) {
        Map<String, String> guests = new HashMap<>();
        guests.put("Jack", "Sara");
        guests.put("Bob", "Alisa");
        guests.put("Frank", "Nancy");

        List<WeddingInvitation> weddingInvitations = new ArrayList<>();

        WeddingInvitation wIPrototype = new WeddingInvitation();
        wIPrototype.setDate(new GregorianCalendar(2021, Calendar.JULY, 27, 17, 0).getTime());
        wIPrototype.setText("You are invited!!!");

        for (Map.Entry<String, String> entry : guests.entrySet()) {
            WeddingInvitation buf = wIPrototype.clone();
            buf.setGuestName(entry.getKey());
            buf.setGuestNamePartner(entry.getValue());
            weddingInvitations.add(buf);
        }
        ;

        weddingInvitations.forEach(System.out::println);
    }

}
