package patterns.creational.Singleton;

public enum EnumSingleton {
    INSTANCE;
    private long number;

    EnumSingleton() {
        number = 0;
    }

    public long getNumber() {
        return number;
    }

    public synchronized void increment() {
        number++;
    }
}
