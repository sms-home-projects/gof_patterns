package patterns.creational.Singleton;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Application {

    public static void main(String[] args) {
        Singleton singleton1 = Singleton.getInstance();
        Singleton singleton2 = Singleton.getInstance();

        System.out.println("1 HASH -> " + singleton1.hashCode());
        System.out.println("2 HASH -> " + singleton2.hashCode());

        ExecutorService executorService = Executors.newFixedThreadPool(3);
        executorService.submit(() -> {
            Singleton singleton3 = Singleton.getInstance();
            for (int i = 0; i < 10000000; i++) {
                singleton3.increment();
            }
        });
        executorService.submit(() -> {
            Singleton singleton4 = Singleton.getInstance();
            for (int i = 0; i < 10000000; i++) {
                singleton4.increment();
            }
        });
        executorService.submit(() -> {
            Singleton singleton5 = Singleton.getInstance();
            for (int i = 0; i < 10000000; i++) {
                singleton5.increment();
            }
        });

        executorService.shutdown();
        while (!executorService.isTerminated()) {
        }

        System.out.println("Value must be 30 000 000 and is " + singleton1.getValue());
        EnumSingleton enumSingleton1 = EnumSingleton.INSTANCE;
        EnumSingleton enumSingleton2 = EnumSingleton.INSTANCE;

        System.out.println("1 HASH -> " + enumSingleton1.hashCode());
        System.out.println("2 HASH -> " + enumSingleton2.hashCode());

        ExecutorService trueExecutorService = Executors.newFixedThreadPool(3);
        trueExecutorService.submit(() -> {
            EnumSingleton enumSingleton3 = EnumSingleton.INSTANCE;
            for (int i = 0; i < 10000000; i++) {
                enumSingleton3.increment();
            }
        });
        trueExecutorService.submit(() -> {
            EnumSingleton enumSingleton4 = EnumSingleton.INSTANCE;
            for (int i = 0; i < 10000000; i++) {
                enumSingleton4.increment();
            }
        });
        trueExecutorService.submit(() -> {
            EnumSingleton enumSingleton5 = EnumSingleton.INSTANCE;
            for (int i = 0; i < 10000000; i++) {
                enumSingleton5.increment();
            }
        });

        trueExecutorService.shutdown();
        while (!trueExecutorService.isTerminated()) {
        }
        System.out.println("Value must be 30 000 000 and is " + enumSingleton1.getNumber());

    }

}
