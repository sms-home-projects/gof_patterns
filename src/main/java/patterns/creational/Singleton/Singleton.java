package patterns.creational.Singleton;

public final class Singleton {
    private static Singleton instance;

    public long value;

    private Singleton() {
        this.value = value;
    }

    public static Singleton getInstance() {

        Singleton singleton = instance;
        if (singleton != null) {
            return singleton;
        }
        synchronized (Singleton.class) {
            if (instance == null) {
                instance = new Singleton();
            }
            return instance;
        }
    }

    public synchronized void increment() {
        value++;
    }

    public long getValue() {
        return value;
    }

}
