package patterns.creational.Builder.travels;

public enum TravelType {
    BEACH, ACTION, CULTURE, ROMANCE
}
