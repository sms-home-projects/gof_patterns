package patterns.creational.Builder.travels;

public class Travel {
    private TravelType travelType;
    private int countOfTravelers;
    private int days;
    private int priceLimit;
    private String comments;

    public Travel(TravelType travelType, int countOfTravelers, int days, int priceLimit, String comments) {
        this.travelType = travelType;
        this.countOfTravelers = countOfTravelers;
        this.days = days;
        this.priceLimit = priceLimit;
        this.comments = comments;
    }

    public TravelType getTravelType() {
        return travelType;
    }

    public void setTravelType(TravelType travelType) {
        this.travelType = travelType;
    }

    public int getCountOfTravelers() {
        return countOfTravelers;
    }

    public void setCountOfTravelers(int countOfTravelers) {
        this.countOfTravelers = countOfTravelers;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public int getPriceLimit() {
        return priceLimit;
    }

    public void setPriceLimit(int priceLimit) {
        this.priceLimit = priceLimit;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return "Travel{" +
                "travelType=" + travelType +
                ", countOfTravelers=" + countOfTravelers +
                ", days=" + days +
                ", priceLimit=" + priceLimit +
                ", comments='" + comments + '\'' +
                '}';
    }
}
