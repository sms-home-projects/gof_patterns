package patterns.creational.Builder.builders;

import patterns.creational.Builder.travels.TravelType;

public interface Builder {
    void setTravelType(TravelType travelType);

    void setCountOfTravelers(int countOfTravelers);

    void setDaysCount(int daysCount);

    void setPriceLimit(int priceLimit);

    void setComments(String comments);

}
