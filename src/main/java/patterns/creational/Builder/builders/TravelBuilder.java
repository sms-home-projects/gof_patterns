package patterns.creational.Builder.builders;

import patterns.creational.Builder.travels.Travel;
import patterns.creational.Builder.travels.TravelType;

public class TravelBuilder implements Builder {
    private TravelType travelType;
    private int countOfTravelers;
    private int days;
    private int priceLimit;
    private String comments;


    @Override
    public void setTravelType(TravelType travelType) {
        this.travelType = travelType;
    }

    @Override
    public void setCountOfTravelers(int travelers) {
        this.countOfTravelers = travelers;
    }

    @Override
    public void setDaysCount(int daysCount) {
        this.days = daysCount;
    }

    @Override
    public void setPriceLimit(int priceLimit) {
        this.priceLimit = priceLimit;
    }

    @Override
    public void setComments(String comments) {
        this.comments = comments;
    }

    public Travel getTravel() {
        return new Travel(travelType, countOfTravelers, days, priceLimit, comments);
    }
}
