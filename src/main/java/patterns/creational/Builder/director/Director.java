package patterns.creational.Builder.director;

import patterns.creational.Builder.builders.Builder;
import patterns.creational.Builder.travels.TravelType;

import java.util.Scanner;

public class Director {
    Scanner sc = new Scanner(System.in);

    private int ask(String msg) {
        System.out.println(msg);
        return sc.nextInt();
    }

    private String askComments() {
        StringBuilder buf = new StringBuilder();
        String str;
        do {
            str = sc.nextLine();
            System.out.println("Do you have any additional wishes? (Press <<0>> if no)");
            if (!str.equals("0")) {
                buf.append(str);
            }
        } while (!str.equals("0"));
        return buf.toString();
    }

    public void constructBeachTravel(Builder builder) {
        builder.setTravelType(TravelType.BEACH);
        builder.setCountOfTravelers(ask("How many travelers?"));
        builder.setDaysCount(ask("How many days?"));
        builder.setPriceLimit(ask("What is the price limit?"));
        builder.setComments(askComments());

    }

    public void constructRomanceTravel(Builder builder) {
        builder.setTravelType(TravelType.ROMANCE);
        builder.setCountOfTravelers(2);
        builder.setDaysCount(ask("How many days?"));
        builder.setPriceLimit(ask("What is the price limit?"));
        builder.setComments(askComments());
    }
}
