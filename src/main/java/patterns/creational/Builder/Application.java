package patterns.creational.Builder;

import patterns.creational.Builder.builders.TravelBuilder;
import patterns.creational.Builder.director.Director;

public class Application {
    public static void main(String[] args) {
        Director director = new Director();

        TravelBuilder beachTravelBuilder = new TravelBuilder();
        TravelBuilder romanceTravelBuilder = new TravelBuilder();

        System.out.println("New Beach Travel created! Please add more details");
        director.constructBeachTravel(beachTravelBuilder);
        System.out.println("New Romance Travel created! Please add more details");
        director.constructRomanceTravel(romanceTravelBuilder);

        System.out.println(beachTravelBuilder.getTravel().toString());
        System.out.println(romanceTravelBuilder.getTravel().toString());
    }
}
