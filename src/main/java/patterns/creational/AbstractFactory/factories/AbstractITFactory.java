package patterns.creational.AbstractFactory.factories;

import patterns.creational.AbstractFactory.specialists.Junior;
import patterns.creational.AbstractFactory.specialists.Trainee;

public interface AbstractITFactory {
    Trainee traineTrainee();

    Junior traineJunior();
}
