package patterns.creational.AbstractFactory.factories;

import patterns.creational.AbstractFactory.specialists.PythonJunior;
import patterns.creational.AbstractFactory.specialists.PythonTrainee;

public class PythonFactory implements AbstractITFactory {
    @Override
    public PythonTrainee traineTrainee() {
        PythonTrainee pythonTrainee = new PythonTrainee();
        pythonTrainee.say();
        return pythonTrainee;
    }

    @Override
    public PythonJunior traineJunior() {
        PythonJunior pythonJunior = new PythonJunior();
        pythonJunior.say();
        return pythonJunior;
    }
}
