package patterns.creational.AbstractFactory.factories;

import patterns.creational.AbstractFactory.specialists.JavaJunior;
import patterns.creational.AbstractFactory.specialists.JavaTrainee;

public class JavaFactory implements AbstractITFactory {
    @Override
    public JavaTrainee traineTrainee() {
        JavaTrainee javaTrainee = new JavaTrainee();
        javaTrainee.say();
        return javaTrainee;
    }

    @Override
    public JavaJunior traineJunior() {
        JavaJunior javaJunior = new JavaJunior();
        javaJunior.say();
        return javaJunior;
    }
}
