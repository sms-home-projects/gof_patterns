package patterns.creational.AbstractFactory.specialists;

public class PythonTrainee implements Trainee, Sayable {
    @Override
    public void say() {
        System.out.println(new PythonTrainee().greeting() + "I am not ready to start developing on Python. I need more study");
    }
}
