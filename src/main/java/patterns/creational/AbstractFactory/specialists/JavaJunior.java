package patterns.creational.AbstractFactory.specialists;

public class JavaJunior implements Junior, Sayable {
    @Override
    public void say() {
        System.out.println(new JavaJunior().greeting() + "I am almost ready to start developing on Java");
    }


    @Override
    public Junior traineJunior() {
        return new JavaJunior();
    }
}
