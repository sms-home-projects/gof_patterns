package patterns.creational.AbstractFactory.specialists;

public interface Junior {
    default String greeting() {
        return "Hello! My level is Junior. ";
    }

    Junior traineJunior();
}
