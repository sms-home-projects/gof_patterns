package patterns.creational.AbstractFactory.specialists;

public interface Sayable {
    void say();
}
