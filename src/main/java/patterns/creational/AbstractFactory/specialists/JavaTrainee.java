package patterns.creational.AbstractFactory.specialists;

public class JavaTrainee implements Trainee, Sayable {
    @Override
    public void say() {
        System.out.println(new JavaTrainee().greeting() + "I am not ready to start developing on Java. I need more study");
    }
}
