package patterns.creational.AbstractFactory.specialists;

public interface Trainee {

    default String greeting() {
        return "Hello! My level is Trainee ";
    }
}
