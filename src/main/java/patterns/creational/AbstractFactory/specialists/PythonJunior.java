package patterns.creational.AbstractFactory.specialists;

public class PythonJunior implements Junior, Sayable {
    @Override
    public void say() {
        System.out.println(new PythonJunior().greeting() + "I am almost ready to start developing on Python");
    }

    @Override
    public Junior traineJunior() {
        return new PythonJunior();
    }
}
