package patterns.creational.AbstractFactory;

import patterns.creational.AbstractFactory.factories.JavaFactory;
import patterns.creational.AbstractFactory.factories.PythonFactory;
import patterns.creational.AbstractFactory.specialists.JavaJunior;
import patterns.creational.AbstractFactory.specialists.JavaTrainee;
import patterns.creational.AbstractFactory.specialists.PythonJunior;
import patterns.creational.AbstractFactory.specialists.PythonTrainee;

public class Application {
    public static void main(String[] args) {
        JavaFactory javaFactory = new JavaFactory();
        PythonFactory pythonFactory = new PythonFactory();

        JavaTrainee javaTrainee = javaFactory.traineTrainee();
        JavaJunior javaJunior = javaFactory.traineJunior();

        PythonTrainee pythonTrainee = pythonFactory.traineTrainee();
        PythonJunior pythonJunior = pythonFactory.traineJunior();

    }
}
