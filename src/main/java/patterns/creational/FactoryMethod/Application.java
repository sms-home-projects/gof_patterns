package patterns.creational.FactoryMethod;

import patterns.creational.FactoryMethod.factory.BoxFactory;

public class Application {
    private BoxFactory boxFactory;

    public static void main(String[] args) {

        RandomBoxes randomBoxes = new RandomBoxes();
        randomBoxes.randomCreator(100);
    }

}
