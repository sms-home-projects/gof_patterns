package patterns.creational.FactoryMethod.factory;

import patterns.creational.FactoryMethod.product.Box;

public interface BoxFactory {
    Box createBox();

}
