package patterns.creational.FactoryMethod.factory;

import patterns.creational.FactoryMethod.product.Box;
import patterns.creational.FactoryMethod.product.PaperBox;

public class PaperBoxFactory implements BoxFactory {
    @Override
    public Box createBox() {
        return new PaperBox();
    }
}
