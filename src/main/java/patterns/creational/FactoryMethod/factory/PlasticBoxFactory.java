package patterns.creational.FactoryMethod.factory;

import patterns.creational.FactoryMethod.product.Box;
import patterns.creational.FactoryMethod.product.PlasticBox;

public class PlasticBoxFactory implements BoxFactory {
    @Override
    public Box createBox() {
        return new PlasticBox();
    }
}
