package patterns.creational.FactoryMethod.product;

public interface Box {
    void open();

    void putInto();

    void close();

    void setLength(int length);

    void setWidth(int width);

    void setHeight(int height);
}
