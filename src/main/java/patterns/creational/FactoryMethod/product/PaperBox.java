package patterns.creational.FactoryMethod.product;

public class PaperBox implements Box {
    private int length;
    private int width;
    private int height;

    @Override
    public void open() {
        System.out.println("Paper box is opened");
    }

    @Override
    public void putInto() {
        System.out.println("Paper box is full");
    }

    @Override
    public void close() {
        System.out.println("Paper box is closed");
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "PaperBox(" +
                length +
                "x" + width +
                "x" + height +
                ") ";
    }
}
