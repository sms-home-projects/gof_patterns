package patterns.creational.FactoryMethod.product;

public class PlasticBox implements Box {
    private int length;
    private int width;
    private int height;

    @Override
    public void open() {
        System.out.println("Plastic box is opened");
    }

    @Override
    public void putInto() {
        System.out.println("Plastic box is full");
    }

    @Override
    public void close() {
        System.out.println("Plastic box is closed");
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "PlasticBox(" +
                length +
                "x" + width +
                "x" + height +
                ") ";
    }
}
