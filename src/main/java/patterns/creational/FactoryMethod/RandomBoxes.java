package patterns.creational.FactoryMethod;

import patterns.creational.FactoryMethod.product.Box;
import patterns.creational.FactoryMethod.product.PaperBox;
import patterns.creational.FactoryMethod.product.PlasticBox;

import java.util.Random;

public class RandomBoxes {
    private Random random = new Random();

    private int length;
    private int width;
    private int height;

    private Box box;

    void randomCreator(int count) {
        for (int i = 0; i < count; i++) {
            int rnd = random.nextInt(2);
            if (rnd == 0) {
                box = new PlasticBox();
            } else {
                box = new PaperBox();
            }
            length = 5 * random.nextInt(20) + 5;
            width = 5 * random.nextInt(20) + 5;
            height = 5 * random.nextInt(10) + 5;

            box.setLength(Math.max(length, width));
            box.setWidth(Math.min(length, width));
            box.setHeight(height);

            System.out.println(box.toString());
        }
    }

}
